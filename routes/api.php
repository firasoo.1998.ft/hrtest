<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HrController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register',[AdminController::class,'register']);
Route::post('login',[AdminController::class,'login']);
Route::post('logout',[AdminController::class,'logout']);

Route::group([
    'prefix' => 'employee_auth'
], function ($router) {
    Route::post('register', [EmployeeController::class, 'register']);
    Route::post('login', [EmployeeController::class, 'login']);
    Route::post('logout', [EmployeeController::class, 'logout']);
});

Route::group([
    'prefix' => 'hr_auth'
], function ($router) {
    Route::post('register', [HrController::class, 'register']);
    Route::post('login', [HrController::class, 'login']);
    Route::post('logout', [HrController::class, 'logout']);
});
